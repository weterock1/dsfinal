from collections import Counter

import graphviz
import pandas as pd
import csv
import matplotlib.pyplot as plt

from scipy import stats
from scipy.spatial.distance import cdist
from sklearn.cluster import KMeans
import numpy as np

from mlxtend.frequent_patterns import apriori
from mlxtend.frequent_patterns import association_rules
from sklearn import tree
from sklearn import preprocessing
from sklearn.cross_validation import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.naive_bayes import GaussianNB
##Read DataFrame from CSV format
ds = pd.read_csv("responses.csv")

##Selected needed columns
newds = pd.DataFrame()
##People_characteristic
newds.insert(loc = len(newds.columns),column = 'gender', value=ds["Gender"])
newds.insert(loc = len(newds.columns),column = 'age', value=ds["Age"])
newds.insert(loc = len(newds.columns),column = 'one_child', value=ds["Only child"])
newds.insert(loc = len(newds.columns),column = 'town', value=ds["Village - town"])
newds.insert(loc = len(newds.columns),column = 'happy', value=ds["Happiness in life"])
newds.insert(loc = len(newds.columns),column = 'height', value=ds["Height"])


##Life_ideas
newds.insert(loc = len(newds.columns),column = 'friends', value=ds["Number of friends"])
newds.insert(loc = len(newds.columns),column = 'god', value=ds["God"])
newds.insert(loc = len(newds.columns),column = 'smoking', value=ds["Smoking"])
newds.insert(loc = len(newds.columns),column = 'alcohol', value=ds["Alcohol"])

##Music
newds.insert(loc = len(newds.columns),column = 'folk', value=ds["Folk"])
newds.insert(loc = len(newds.columns),column = 'country', value=ds["Country"])
newds.insert(loc = len(newds.columns),column = 'classic', value=ds["Classical music"])
newds.insert(loc = len(newds.columns),column = 'pop', value=ds["Pop"])
newds.insert(loc = len(newds.columns),column = 'rock', value=ds["Rock"])
newds.insert(loc = len(newds.columns),column = 'metal', value=ds["Metal or Hardrock"])
newds.insert(loc = len(newds.columns),column = 'punk', value=ds["Punk"])
newds.insert(loc = len(newds.columns),column = 'hiphop', value=ds["Hiphop, Rap"])
newds.insert(loc = len(newds.columns),column = 'ska', value=ds["Reggae, Ska"])
newds.insert(loc = len(newds.columns),column = 'jazz', value=ds["Swing, Jazz"])
newds.insert(loc = len(newds.columns),column = 'rockroll', value=ds["Rock n roll"])
newds.insert(loc = len(newds.columns),column = 'alt', value=ds["Alternative"])
newds.insert(loc = len(newds.columns),column = 'latino', value=ds["Latino"])
newds.insert(loc = len(newds.columns),column = 'trance', value=ds["Techno, Trance"])
newds.insert(loc = len(newds.columns),column = 'opera', value=ds["Opera"])
##Movies
newds.insert(loc = len(newds.columns),column = 'horror', value=ds["Horror"])
newds.insert(loc = len(newds.columns),column = 'triler', value=ds["Thriller"])
newds.insert(loc = len(newds.columns),column = 'comedy', value=ds["Comedy"])
newds.insert(loc = len(newds.columns),column = 'romantic', value=ds["Romantic"])
newds.insert(loc = len(newds.columns),column = 'western', value=ds["Western"])
newds.insert(loc = len(newds.columns),column = 'document', value=ds["Documentary"])
newds.insert(loc = len(newds.columns),column = 'animate', value=ds["Animated"])
newds.insert(loc = len(newds.columns),column = 'tales', value=ds["Fantasy/Fairy tales"])
newds.insert(loc = len(newds.columns),column = 'war', value=ds["War"])
newds.insert(loc = len(newds.columns),column = 'scifi', value=ds["Sci-fi"])


## Make zero (for noFear people) provided in description
d = {None : 0, 1:1,2:2,3:3,4:4,5:5}
ds['Flying'] = ds['Flying'].map(d)
ds["Storm"] = ds["Storm"].map(d)
ds["Darkness"] = ds["Darkness"].map(d)
ds["Heights"] = ds["Heights"].map(d)
ds["Spiders"] = ds["Spiders"].map(d)
ds["Snakes"] = ds["Snakes"].map(d)
ds["Rats"] = ds["Rats"].map(d)
ds["Ageing"] = ds["Ageing"].map(d)
ds["Dangerous dogs"] = ds["Dangerous dogs"].map(d)
ds["Fear of public speaking"] = ds["Fear of public speaking"].map(d)

##Index of Fear
newds.insert(loc = len(newds.columns),column = 'fear', value=(ds['Flying'] + ds["Storm"] + ds["Darkness"] + ds["Heights"]
                                                              + ds["Spiders"] + ds["Snakes"] + ds["Rats"] + ds["Ageing"]
                                                              + ds["Dangerous dogs"] + ds["Fear of public speaking"]))
##Specify data
d = {None:0, 'never smoked':1,'tried smoking':3,'former smoker': 2, 'current smoker' : 4}
newds['smoking'] = newds['smoking'].map(d)
d = {None:0, 'never':1,'social drinker': 2, 'drink a lot' : 3}
newds['alcohol'] = newds['alcohol'].map(d)
d = {'village':0,'city':1}
newds['town'] = newds['town'].map(d)

##(Create subset for prediction)
tset = pd.DataFrame()
tset.insert(loc = len(tset.columns),column = 'age', value=newds["age"])
tset.insert(loc = len(tset.columns),column = 'gender', value=newds["gender"])
tset.insert(loc = len(tset.columns),column = 'friends', value=newds["friends"])
tset.insert(loc = len(tset.columns),column = 'god', value=newds["god"])
tset.insert(loc = len(tset.columns),column = 'smoking', value=newds["smoking"])
tset.insert(loc = len(tset.columns),column = 'alcohol', value=newds["alcohol"])
tset.insert(loc = len(tset.columns),column = 'fear', value=newds["fear"])
tset.insert(loc = len(tset.columns),column = 'happy', value=newds["happy"])
tset.insert(loc = len(tset.columns),column = 'town', value=newds["town"])
tset.insert(loc = len(tset.columns),column = 'height', value=newds["height"])

vc = tset['happy'].value_counts()
vc = vc.sort_index()
vc.plot(kind='bar')
plt.title("Distribution of happy at start")
plt.show()

vc = tset['age'].value_counts()
vc = vc.sort_index()
vc.plot(kind='bar')
plt.title("Distribution of age")
plt.show()

vc = tset['gender'].value_counts()
vc = vc.sort_index()
vc.plot(kind='bar')
plt.title("Distribution of gender")
plt.show()


vc = newds['classic'].value_counts()
vc = vc.sort_index()
vc.plot(kind='bar')
plt.title("Distribution lover of classic-music")
plt.show()


vc = newds['metal'].value_counts()
vc = vc.sort_index()
vc.plot(kind='bar')
plt.title("Distribution lover of metal-music")
plt.show()


vc = newds['smoking'].value_counts()
vc = vc.sort_index()
vc.plot(kind='bar')
plt.title("Distribution of smokers")
plt.show()
#Index of happy
d = {1:0,2:1,3:1,4:1,5:1}
tset['happy'] = tset['happy'].map(d)
##Delete paranoics from happy
mas = tset['happy']
for index,row in tset.iterrows():
    if (row['fear']>32):
        mas[index] = 0
tset['happy'] = mas

vc = tset['happy'].value_counts()
vc = vc.sort_index()
vc.plot(kind='bar')
plt.title("Distribution of happy for analyze")
plt.show()
d = {'female': 0, 'male': 1}
tset['gender'] = tset['gender'].map(d)
##Delete None
tset = tset.dropna()

#T_test
# groupby_gender = tset.groupby('happy')
# groupby_gender['age'].plot(kind='kde', legend=True )
# plt.show()

# f_val, p_val = stats.f_oneway(tset[tset['happy'] == 0]['age'],
#                               tset[tset['happy'] == 1]['age'])
# print (f_val, "happy age One-way ANOVA P =", p_val)
#
# f_val, p_val = stats.f_oneway(tset[tset['happy'] == 0]['smoking'],
#                               tset[tset['happy'] == 1]['smoking'])
# print (f_val, "happy smoking One-way ANOVA P =", p_val)
#
# f_val, p_val = stats.f_oneway(tset[tset['happy'] == 0]['friends'],
#                               tset[tset['happy'] == 1]['friends'])
# print (f_val, "happy friends One-way ANOVA P =", p_val)
#
# f_val, p_val = stats.f_oneway(tset[tset['happy'] == 0]['god'],
#                               tset[tset['happy'] == 1]['god'])
# print (f_val, "happy god One-way ANOVA P =", p_val)
#
# f_val, p_val = stats.f_oneway(tset[tset['happy'] == 0]['alcohol'],
#                               tset[tset['happy'] == 1]['alcohol'])
# print (f_val, "happy alcohol One-way ANOVA P =", p_val)
# ANOVA test
f_val, p_val = stats.f_oneway(tset[tset['alcohol'] == 0]['smoking'],
                               tset[tset['alcohol'] == 1]['smoking'],
                               tset[tset['alcohol'] == 2]['smoking'],
                               tset[tset['alcohol'] == 3]['smoking'],)
print (f_val, "happy height One-way ANOVA P =", p_val)

groupby_gender = tset.groupby('alcohol')
groupby_gender['smoking'].plot(kind='kde', legend=True )
plt.show()
#
# groupby_gender = tset.groupby('happy')
# groupby_gender['god'].plot(kind='kde', legend=True )
# plt.show()
#
# female_viq = tset[tset['happy'] == 1]['height']
# male_viq = tset[tset['happy'] == 0]['height']
# print(stats.ttest_ind(female_viq, male_viq))
#
# female_viq = newds[newds['gender'] == 'female']['smoking']
# male_viq = newds[newds['gender'] == 'male']['smoking']
# print(stats.ttest_ind(female_viq, male_viq))
#
# groupby_gender['alcohol'].plot(kind='kde', legend=True )
# plt.show()
# female_viq = newds[newds['gender'] == 'female']['alcohol']
# male_viq = newds[newds['gender'] == 'male']['alcohol']
# print(stats.ttest_ind(female_viq, male_viq))


##Associations_Rules SubSet
asrul = pd.DataFrame()
asrul.insert(loc = len(asrul.columns),column = 'folk', value=newds["folk"])
asrul.insert(loc = len(asrul.columns),column = 'country', value=newds["country"])
asrul.insert(loc = len(asrul.columns),column = 'classic', value=newds["classic"])
asrul.insert(loc = len(asrul.columns),column = 'pop', value=newds["pop"])
asrul.insert(loc = len(asrul.columns),column = 'rock', value=newds["rock"])
asrul.insert(loc = len(asrul.columns),column = 'metal', value=newds["metal"])
asrul.insert(loc = len(asrul.columns),column = 'punk', value=newds["punk"])
asrul.insert(loc = len(asrul.columns),column = 'hiphop', value=newds["hiphop"])
asrul.insert(loc = len(asrul.columns),column = 'ska', value=newds["ska"])
asrul.insert(loc = len(asrul.columns),column = 'jazz', value=newds["jazz"])
asrul.insert(loc = len(asrul.columns),column = 'rockroll', value=newds["rockroll"])
asrul.insert(loc = len(asrul.columns),column = 'alt', value=newds["alt"])
asrul.insert(loc = len(asrul.columns),column = 'latino', value=newds["latino"])
asrul.insert(loc = len(asrul.columns),column = 'trance', value=newds["trance"])
asrul.insert(loc = len(asrul.columns),column = 'opera', value=newds["opera"])

asrul.insert(loc = len(asrul.columns),column = 'horror', value=newds["horror"])
asrul.insert(loc = len(asrul.columns),column = 'triler', value=newds["triler"])
asrul.insert(loc = len(asrul.columns),column = 'comedy', value=newds["comedy"])
asrul.insert(loc = len(asrul.columns),column = 'romantic', value=newds["romantic"])
asrul.insert(loc = len(asrul.columns),column = 'western', value=newds["western"])
asrul.insert(loc = len(asrul.columns),column = 'document', value=newds["document"])
asrul.insert(loc = len(asrul.columns),column = 'animate', value=newds["animate"])
asrul.insert(loc = len(asrul.columns),column = 'tales', value=newds["tales"])
asrul.insert(loc = len(asrul.columns),column = 'war', value=newds["war"])
asrul.insert(loc = len(asrul.columns),column = 'scifi', value=newds["scifi"])

asrul = asrul.dropna()
d = {1:0,2:0,3:0,4:1,5:1}
for i in list(asrul):
    asrul[i] = asrul[i].map(d)
##Associations rules with the define parametrs of sup,lift,conv,lev
freq = apriori(asrul, min_support=0.08, use_colnames=True)
rules = association_rules(freq, metric="lift", min_threshold=1.5)
rules =rules[ (rules['confidence'] > 0.75) & (rules['leverage'] > 0.05)]
print(rules)

#K-mean cluster
print(tset)
X = np.array(list(zip(tset['age'].values,
                      tset['height'].values,
                      tset['friends'].values,
                      tset['god'].values,
                      tset['town'].values)))
##plt.scatter(f1, f2, s=2)
##plt.show()

distortions = []
K = range(1, 20)
for k in K:
    kmeanModel = KMeans(n_clusters=k).fit(X)
    kmeanModel.fit(X)
    distortions.append(sum(np.min(cdist(X, kmeanModel.cluster_centers_, 'euclidean'), axis=1)) / X.shape[0])

# Plot the elbow
plt.plot(K, distortions, 'bx-')
plt.xlabel('k')
plt.ylabel('Distortion')
plt.title('The Elbow Method showing the optimal k')
plt.show()

kmeans = KMeans(n_clusters=10)
kmeans.fit(X)
y_kmeans = kmeans.predict(X)
print(y_kmeans)

##Cluster is not provided true sight of clusterization due dimension of data
plt.scatter(X[:, 0], X[:, 1], c=y_kmeans, s=50, cmap='viridis')

centers = kmeans.cluster_centers_
plt.scatter(centers[:, 0], centers[:, 1], c='black', s=200, alpha=0.5)
plt.show()


#Naive Bayes and Decision tree provided both of them for the comparing

features = tset.values[:,:6]
target = tset.values[:,7]

features_train, features_test, target_train, target_test = train_test_split(features,
target, test_size = 0.2, random_state = 10)

#NB
clf = GaussianNB()
clf = clf.fit(features_train, target_train)
target_pred = clf.predict(features_test)
accuracy = accuracy_score(target_test, target_pred, normalize = True)
print('NB accuracy = ', accuracy)

#DT
clf = tree.DecisionTreeClassifier()
clf = clf.fit(features_train, target_train)
target_pred = clf.predict(features_test)
accuracy = accuracy_score(target_test, target_pred, normalize = True)
print('DT accuracy = ', accuracy)
#predic1t = clf.predict(features_train[18].reshape(1,-1))

graph_data = tree.export_graphviz(clf, out_file=None,
                         feature_names=['age','gender','friends','god','smoking','alcohol'],
                         class_names=['happy','unhappy'],
                         filled=True, rounded=True,
                         special_characters=True)
graph = graphviz.Source(graph_data)
graph.render("Decision tree")
